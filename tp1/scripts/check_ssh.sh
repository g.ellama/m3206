dpkg -l ssh >> /dev/null 2>&1

if [ $? -eq 0 ]; then
	echo "[...] ssh: est installé  [...]"
else
	echo "[...] ssh: n'est pas installé  [...]"
fi

ps aux | grep [s]shd >> /dev/null 2>&1

if [ $? -eq 0 ]; then
	echo "[...] ssh: est lancé  [...]"
else
	echo "[...] ssh: n'est pas lancé  [...]"
	echo "[...] ssh: lancement  [...]"
	/etc/init.d/ssh start
fi


