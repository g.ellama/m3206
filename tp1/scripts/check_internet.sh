#Affichage texte pour dire à l'utilisateur que l'on test ça connection internet
echo "[...] Checking internet connection [...]"
#Test de téléchargement de la page https://google.com sans afficher les information de téléchargment et avec un temps imposé 
wget -q --tries=10 --timeout=20 --spider https://google.com
#Début de la boucle if else qui va afficher deux message texte différent en fonction de l'aboutissement ou non du téléchargement effectué précédemment
if [[ $? -eq 0 ]]; then
	echo "[...]Internet access OK      [...]"
else 
	echo "[/!\]Not connected to Internet [/!\]  "
	echo "[/!\] Please check configuration [/!\]"
fi
