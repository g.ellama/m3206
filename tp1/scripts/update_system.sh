#On commence par vérifier que l'on est bien super utilisateur
if [[ $EUID = 0 ]]; then
#On exécute apt-get update et on fait en sorte qu'il n'y ai pas d'affichage des téléchargements effectués
echo "[...] update database [...]"
apt-get update >> /dev/null 2>&1
#De même pour apt-get upgrage
echo "[...] upgrade system  [...]"
apt-get upgrade >> /dev/null 2>&1
else
#Sinon on annonce que l'on n'est pas en super utilisateur
echo "[/!\] Vous devez être super-utilisateur [/!\]"
fi
