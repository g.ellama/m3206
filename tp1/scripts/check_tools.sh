#On vérifie que le programme est installé
if ! which git > /dev/null; then 
	echo "[...] git: non-installé    [...]"
#on prévient l'utilisateur que l'on lance l'installation et on installe
	echo "[...] Installation git    [...]"
	apt-get install git
#Sinon on indique à l'utilisateur que le programme est déjà installé
else
	echo "[...] git: installé    [...]"
fi

if ! which tmux > /dev/null; then 
	echo "[...] tmux: non-installé    [...]"
	echo "[...] Installation tmux    [...]"
	apt-get install tmux
else
	echo "[...] tmux: installé    [...]"
fi

if ! which vim > /dev/null; then 
	echo "[...] vim: non-installé    [...]"
	echo "[...] Installation vim    [...]"
	apt-get install vim
else
	echo "[...] vim: installé    [...]"
fi

if ! which htop > /dev/null; then 
	echo "[...] htop: non-installé    [...]"
	echo "[...] Installation htop    [...]"
	apt-get install htop
else
	echo "[...] htop: installé    [...]"
fi
