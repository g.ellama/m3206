## M3206 Automatisation des tâches d'administration
__Exercice sur find__ 

La commande suivante : 

```bash
find /tmp -name ".txt" -ok rm {} \;
```
permet de supprimer les fichier comportant l'extension .txt en demandant à l'utilisateur pour chaque fichier de ce type s'il est d'accord ou non.


L'option -ok permet de demander à l'utilisateur la confirmation pour chaque fichier.


La commande cp permet de faire des sauvegardes de fichier ou de dossier.


La commande qui permet de lister les premières lignes de tous les fichiers .txt d'un répertoire est : 
```bash 
find /tmp -name ".txt" -exec head -3 {} \;
```

Ainsi on obtiendra les 3 premières lignes de tous les fichier .txt


