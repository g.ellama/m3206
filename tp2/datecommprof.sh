#!/bin/bash
f="first"
l="last"

if [ $1 = $f ]; then 
temps=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ':' -f1 | head -1)
fi

if [ $1 = $l ]; then
temps=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ':' -f1 | sort -r | head -1)
fi

date -d @$temps
